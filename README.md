# Meili agent

## Prerequisites. 

1. Python3-pip:

```
sudo apt-get install python3-pip
```

2. Catkin:


For ROS Melodic:

```
sudo apt-get install ros-melodic-catkin
```

For ROS noetic:

```
sudo apt-get install ros-noetic-catkin
```

3. Rosdep

```
sudo apt-get install python-rosdep
``` 

## Installation

1. Clone repository in your ros_ws inside src folder

2. Install python dependencies

```
cd ros_ws/src/meili-agent-noetic/src

pip install -r requirements.txt
```

3. Install ros dependencies

```
cd ros_ws

rosdep install --from-paths src --ignore-src -r -y
```

4. Set up config file.

	1. Create config.yaml file inside config folder

		```
		cd meili-agent-noetic/config

		touch config.yaml

		```

	2. Config file contain information about the fleet, specific vehicles and MQTT auth. This information need to be configured using Meili Cloud. An example of config file:

```
vehicles:

	uuid: bae59388be704a6e89e5c8a7907b74243
	token:71876edfe6ea939e696f85158c0c7516187136e94
	prefix:

fleet: df1d4b9df56fe55fc43e34e16e533d3df050fa86
mqqt_id: meili-agent-65e49378-f100-5311-918a-11f18ac51fbb
```

5. Rebuild ros enviroment:

```
cd ros_ws

catkin_make
```

## Running

```
roslaunch meili_agent meili_agent.launch
```

## Formatting

1. Install pre-commit from https://pre-commit.com/

2. Run `pre-commit install`

3. Happy committing!

## For WAYPOINTS

WAYPOINTS in ROS functions using a STATE MACHINE that presents three states:

- GetPath: where it is checked that the waypoints[] is not empty. If it is not empty the path is ready.

- FollowPath: an Action Client is created for the MoveBaseAction server. This server will depend on the vehicle prefix. For example: /robot0/move_base or /robot1/move_base.
Each goal of the waypoints array is sent until finalized.

-PathComplete: once all the goals have been reached the PathComplete state starts. This state ends the state machine.

Additional functionalities to cancel or reset the waypoints are set, however they are not used. Maybe for the future some will be interesting to be implemented to add complexity to the state machine decision making,

To run the waypoints, it is important that the following section is uncomment in the file `ws.py`. This allows a fake_waypoint_generator to include two additional goals with respect to the original task, generating an array of goals.


```shell
################# UNCOMMENT THIS LINES WHEN WANTING TO TEST WAYPOINTS #################
x_w, y_w, xm_w, ym_w, rotation_w = self.fake_waypoints_generator(x, y, xm, ym, rotation)

self.waypoints_handler(
    vehicle=vehicle, x=x_w, y=y_w, xm=xm_w, ym=ym_w, rotation=rotation_w

)
########################################################################################
            
```

Finally, run the agent like normal and you will see the following output:
```shell
roslaunch turtlebot3_gazebo turtlebot3_house.launch
roslaunch turtlebot3_navigation turtlebot3_navigation.launch map_file:=/home/.../maps/map-full.yaml
roslaunch meili_agent meili_agent.launch             
```

OUTPUT:
```shell
[INFO] [1628159411.714377, 60.477000]: [ROSAgent] Topic /move_base is configure for vehicle 11dd669722734298b6f5242c48e7c6d6
[ DEBUG ] : Adding state (GET_PATH, <follow_waypoints.GetPath object at 0x7f47127e0a00>, {'success': 'FOLLOW_PATH'})
[ DEBUG ] : Adding state 'GET_PATH' to the state machine.
[ DEBUG ] : State 'GET_PATH' is missing transitions: {}
[ DEBUG ] : TRANSITIONS FOR GET_PATH: {'success': 'FOLLOW_PATH'}
[ DEBUG ] : Adding state (FOLLOW_PATH, <follow_waypoints.FollowPath object at 0x7f47127e06d0>, {'success': 'PATH_COMPLETE'})
[ DEBUG ] : Adding state 'FOLLOW_PATH' to the state machine.
[ DEBUG ] : State 'FOLLOW_PATH' is missing transitions: {}
[ DEBUG ] : TRANSITIONS FOR FOLLOW_PATH: {'success': 'PATH_COMPLETE'}
[ DEBUG ] : Adding state (PATH_COMPLETE, <follow_waypoints.PathComplete object at 0x7f47127e0970>, {'success': 'end'})
[ DEBUG ] : Adding state 'PATH_COMPLETE' to the state machine.
[ DEBUG ] : State 'PATH_COMPLETE' is missing transitions: {}
[ DEBUG ] : TRANSITIONS FOR PATH_COMPLETE: {'success': 'end'}
[  INFO ] : State machine starting in initial state 'GET_PATH' with userdata: 
	['server']
[INFO] [1628159411.732828, 60.491000]: Path Ready
[  INFO ] : State machine transitioning 'GET_PATH':'success'-->'FOLLOW_PATH'
[INFO] [1628159411.769716, 60.519000]: Connecting to /move_base...
[INFO] [1628159411.947589, 60.650000]: Connected to /move_base
[INFO] [1628159411.951432, 60.651000]: Starting a tf listener.
[INFO] [1628159411.973933, 60.658000]: Executing move_base goal to position (x,y): -0.55, 2.5
[INFO] [1628159428.608714, 76.272000]: Waiting for 0.000000 sec...
[INFO] [1628159428.613411, 76.276000]: Executing move_base goal to position (x,y): -0.050000000000000044, 3.0
[INFO] [1628159470.824908, 114.631000]: Waiting for 0.000000 sec...
[INFO] [1628159470.828604, 114.636000]: Executing move_base goal to position (x,y): 0.44999999999999996, 3.5
[INFO] [1628159534.089987, 176.040000]: Waiting for 0.000000 sec...
[  INFO ] : State machine transitioning 'FOLLOW_PATH':'success'-->'PATH_COMPLETE'
[INFO] [1628159534.094006, 176.044000]: ##### REACHED LAST WAYPOINT #####
[  INFO ] : State machine terminating 'PATH_COMPLETE':'success':'end'
             
```


