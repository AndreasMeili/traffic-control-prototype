# Copyright 2018 Amazon.com, Inc. or its affiliates. All Rights Reserved.
# SPDX-License-Identifier: Apache-2.0
from setuptools import find_packages, setup

# Package meta-data.
NAME = "meili_agent"
REQUIRES_PYTHON = ">=3.5.0"

setup(
    name=NAME,
    version="0.5.0",
    zip_safe=False,
    packages=find_packages(),
    python_requires=REQUIRES_PYTHON,
    install_requires=[
        "autopep8==1.5.2",
        "pycodestyle==2.6.0",
        "PyYAML==5.3.1",
        "six==1.13.0",
        "websocket-client==1.2.3",
        "rospkg==1.2.8",
        "influxdb-client==1.16.0",
    ],
    entry_points={
        "console_scripts": [
            "run_meili_agent=agent:main",
            "run_meili_agent_topic_streaming=meili_agent_topic_streaming:main",
        ],
    },
)
