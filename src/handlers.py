""" Handlers for the incoming info from Meili SDK"""
# !/usr/bin/env python3

import math

# ROS1 dependencies
import threading
import time

import rospy

# Meili agent dependencies
from meili_agent.msg import Topicinit
from meili_sdk.websockets.client import MeiliWebsocketClient
from sentry_sdk import capture_exception, capture_message

from agent import agent_sentry
from maths import euler_to_quaternion
from parse_data import parse_cancel_goal, parse_goal, parse_waypoints, parse_vel
from sentry import initialize_sentry

from traffic_control import traffic_control


initialize_sentry()

tc = traffic_control()


def goal_setup(rotation, x_meters, y_meters):
    """Set up the goal with rotation, x and y"""
    try:
        rotation_float = float(rotation)

        if float(rotation) > 180:
            rotation_float = -360 + float(rotation)
        elif float(rotation) == 180:
            rotation_float = -180

        # rotation_quaternion = {"qx": 0, "qy": 0, "qw": 0, "qz": 0}
        rotation_quaternion = euler_to_quaternion(
            0, 0, math.radians(rotation_float))
        goal = parse_goal(x_meters, y_meters,
                          rotation_quaternion=rotation_quaternion)

    except TypeError as error:
        rospy.loginfo("[ROSHandler] Goal Setup Error: %s ", TypeError)
        capture_exception(error)
        raise

    return goal


def get_list_of_topic():
    """Getting the list of topics available with specified namespace"""
    # getting  topics available
    topic_list = rospy.get_published_topics(namespace="/robot1")
    return topic_list


def topic_request_handler(_):
    """logging information"""
    rospy.loginfo("[ROSHandler] Received topic request")
    get_list_of_topic()


class SDKHandlers:
    """Class for all the handlers of messages coming from SDK"""

    def __init__(self, agent):

        self.agent = agent
        self.start_slow_down = []
        self.slow_down = []
        for i in range(0, self.agent.number_of_vehicles):
            self.start_slow_down.append(None)
            self.slow_down.append(False)

    def sdk_setup(self):
        """Setup the Meili Websocket client SDK"""
        # sdk_setup
        try:
            self.agent.client = MeiliWebsocketClient(
                self.agent.token,
                override_host=self.agent.server_instance.replace("http", "ws"),
                fleet=self.agent.fleet,
                open_handler=self.open_handler,
                close_handler=self.close_handler,
                error_handler=self.error_handler,
                task_handler=tc.task_handler,   # use the traffic control task handler
                task_cancellation_handler=self.task_cancellation_handler,
                topic_list_initializer_handler=self.topic_handler,
                move_action_handler=self.move_action_handler,
                slow_down_handler=self.slow_down_handler,
            )

            rospy.loginfo(
                "[ROSHandler] Server instance: %s", self.agent.server_instance
            )
        except KeyError as error:
            rospy.loginfo(f"[ROSHandler] No handler named {error}")
            capture_exception(error)
        return self.agent.client

    def __check_fleet_get_vehicle_position(self, vehicle):
        if self.agent.fleet:
            return self.agent.return_vehicle_position(vehicle)
        return 0

    def move_action_handler(self, _, data: dict, vehicle: str):
        """When a move_action is received, goal is generated and sent to the specific ...
        robot to the charging station"""
        try:

            rate = rospy.Rate(1)
            vehicle_position = self.__check_fleet_get_vehicle_position(vehicle)
            self.send_goal(data, vehicle_position)
            rate.sleep()

            # If the robot is moving the task will be automatically canceled to go to the
            # charging station.
            rospy.loginfo(
                f"[ROSHandler] Sending vehicle {self.agent.vehicle_list[vehicle_position]} to CHARGING STATION"
            )

        except TypeError as error:
            rospy.logerr("[ROSHandler] Error move_action_handler: %s", error)
            agent_sentry(error, "[ROSHandler] Error move_action_handler")

    def task_handler(self, _, data: dict, vehicle: str):
        """When a task is received, goal is generated and sent to the specific robot"""
        try:
            # When a task is received, goal is generated and sent to the specific robot
            vehicle_position = self.__check_fleet_get_vehicle_position(vehicle)
            current_task_uuid = data["uuid"]

            rate = rospy.Rate(1)
            self.agent.number_of_tasks = self.agent.number_of_tasks + 1
            self.agent.task_started[vehicle_position] = 0

            rospy.loginfo(
                "[ROSHandler] Number of received task is: %d ",
                self.agent.number_of_tasks,
            )
            rospy.loginfo(
                f"[ROSHandler] Received task assigned to vehicle: {self.agent.vehicle_list[vehicle_position]}"
            )

            if not ("locations" in data.keys()):
                # This is not a batch-task vehicle
                if (
                        data["metric_waypoints"] is not None
                        and len(data["metric_waypoints"]) > 2
                        and self.agent.node.path_planning
                ):
                    # This task has some intermediate waypoint
                    pose = parse_waypoints(data)
                    self.setup_waypoints_pose(
                        pose,
                        vehicle_position,
                        current_task_uuid,
                    )
                else:
                    self.send_goal(data, vehicle_position)
            elif not data["metric_waypoints"]:
                # Case when only one task is sent
                self.send_goal(data, vehicle_position)
            else:
                # This is batch-task vehicle
                pose = parse_waypoints(data)
                self.setup_waypoints_pose(
                    pose, vehicle_position, current_task_uuid)

            rate.sleep()

        except Exception as error:
            rospy.logerr("[ROSHandler] Error task_handler: %s", error)
            agent_sentry(error, "[ROSHandler] Error task_handler")

    def send_goal(self, data, vehicle_position):
        """Send Goal with data and vehicle position as inputs"""
        x_meters = data["location"]["metric"]["x"]
        y_meters = data["location"]["metric"]["y"]
        try:
            rotation = data["location"]["rotation"]
        except KeyError:
            rotation = 0
        goal = goal_setup(rotation, x_meters, y_meters)
        self.agent.goal_publisher[vehicle_position].publish(goal)

    def setup_waypoints_pose(self, pose, vehicle_position, current_task_uuid):
        """Set up the waypoints from pose information"""

        self.waypoints_handler(
            vehicle_position=vehicle_position,
            pose=pose,
            current_task_uuid=current_task_uuid,
        )

    def waypoints_handler(self, vehicle_position, pose, current_task_uuid):
        """Handles offline waypoints and sends to specific robot"""
        x_meters = [item[0] for item in pose]
        y_meters = [item[1] for item in pose]

        try:
            rotation = [item[2] for item in pose]
        except IndexError:
            rotation = [0 for _ in pose]
        try:
            rate = rospy.Rate(1)
            number_of_waypoints = len(x_meters)

            self.agent.waypoints[vehicle_position] = True
            self.agent.current_task_uuid = current_task_uuid  # master

            count = 0
            while count < number_of_waypoints:
                if self.agent.task_started[vehicle_position] == 0:
                    rospy.loginfo(
                        "[ROSHandler] Number of received waypoints is: %d/%d ",
                        count + 1,
                        number_of_waypoints,
                    )
                    goal = goal_setup(
                        rotation[count], x_meters[count], y_meters[count])
                    self.agent.goal_publisher[vehicle_position].publish(goal)
                    rate.sleep()
                    self.agent.task_started[vehicle_position] = 1
                    self.agent.number_of_tasks += count
                    count += 1

            self.agent.waypoints[vehicle_position] = False

        except Exception as error:
            rospy.logerr("[ROSHandler] Error waypoints_handler: %s", error)
            agent_sentry(error, "[ROSHandler] Error waypoints_handler")

    def task_cancellation_handler(self, goal_id: str, vehicle: str):
        """Handles task cancellation info to specific robot"""
        rospy.loginfo(f"[ROSHandler] Cancelling task with goal id: {goal_id}")
        rospy.loginfo(f"[ROSHandler] Cancelling task of vehicle: {vehicle}")

        # if cancellation is received cancel goal
        vehicle_position = self.__check_fleet_get_vehicle_position(vehicle)

        goal_cancel = parse_cancel_goal(goal_id)
        self.agent.goal_canceller[vehicle_position].publish(goal_cancel)

        # Refresh variables
        self.agent.waypoints[vehicle_position] = False
        self.agent.waypoints_goal_id[vehicle_position] = None

    def close_handler(self):
        """Close the websocket"""
        self.agent.ws_open = False

        if not self.agent.offline:
            rospy.loginfo("[ROSHandler] WS close")
            capture_message("[ROSHandler] WS close")
            rospy.loginfo("[ROSHandler] WS close")
            self.agent.offline = True

    def error_handler(self, _, err):
        """verifies websocket connection"""
        if not self.agent.offline:
            rospy.loginfo("[ROSHandler] WS error: %s ", err)
            capture_message("[ROSHandler] WS error")

    def open_handler(self):
        """Opens the websocket connection"""
        rospy.loginfo("[ROSHandler] WS open")
        self.agent.ws_open = True

    def topic_handler(self, _, topics, vehicle):
        """find vehicle token to passed to the topic streaming node"""

        if (topics and vehicle) is not None:

            vehicle_position = self.__check_fleet_get_vehicle_position(vehicle)
            # initialize custom msg to send topic info
            msg = Topicinit()

            for index in topics["data"]:
                self.agent.total_topics += 1

                self.agent.msg_type = index["message_type"]
                index["vehicle_uuid"] = vehicle

                msg.vehicle_token = vehicle
                msg.topic_uuid = index["uuid"]
                msg.topic_name = index["topic"]
                msg.topic_type = index["message_type"]

                msg.mqtt_id = self.agent.mqtt_id
                msg.frequency = int(self.agent.node.publish_frequency)
                msg.mode = self.agent.node.topic_streaming_node

                msg.total_count = self.agent.total_topics
                msg.number_of_vehicles = vehicle_position + 1
                msg.prefix = self.agent.vehicles[vehicle_position]["prefix"]

                self.agent.topic_streaming_talker(msg)
                rospy.loginfo(
                    "[ROSHandler] Topic to stream %s for vehicle %s",
                    index["topic"],
                    vehicle,
                )
        else:
            rospy.loginfo("[ROSHandler] No topics to stream")

    def slow_down_handler(self, _, data: dict, vehicle: str):
        """Handles slowdown messages and sends to specific vehicle parameters"""
        if self.agent.node.traffic_control:
            max_vel_x = data["data"]["max_vel_x"]
            max_vel_theta = data["data"]["max_vel_theta"]
            vehicle_position = self.__check_fleet_get_vehicle_position(vehicle)
            rospy.loginfo(
                f"[ROSHandler] Collision situation detected for vehicle number: {vehicle_position} ")
            vel = parse_vel(max_vel_x)
            x = threading.Thread(target=self.velocity_publisher, args=(
                vel, vehicle_position), daemon=True)
            threads = []
            for index in range(self.agent.number_of_vehicles):
                threads.append(x)
            self.start_slow_down[vehicle_position] = time.time()
            if not self.slow_down[vehicle_position]:
                self.slow_down[vehicle_position] = True
                threads[vehicle_position].start()

    def velocity_publisher(self, vel, vehicle_position):
        frequency = 50
        rate = rospy.Rate(frequency)
        rospy.loginfo(
            f"[ROSHandler] Stopping vehicle number: {vehicle_position}")
        self.agent.vehicle_count_speed_mod[vehicle_position] = vehicle_position
        self.agent.speed_changed[vehicle_position] = True
        while (time.time() - self.start_slow_down[vehicle_position]) < 8:
            self.agent.cmd_vel_publisher[vehicle_position].publish(vel)
            rate.sleep()

        self.slow_down[vehicle_position] = False
        rospy.loginfo(
            f"[ROSHandler] Restarting vehicle number: {vehicle_position}")
