import rospy
from actionlib_msgs.msg import GoalID
from geometry_msgs.msg import PoseStamped, Twist


def parse_battery_data(msg):
    # creating dictionary for storing battery data
    battery_data = {}

    if hasattr(msg, "capacity"):
        battery_data["value"] = round(msg.capacity, 2)
        battery_data["power_supply_status"] = msg.power_supply_status
    elif hasattr(msg, "level"):
        battery_data["value"] = round(msg.level, 2)
        if msg.is_charging:
            battery_data["power_supply_status"] = 1
        else:
            battery_data["power_supply_status"] = 3
    elif hasattr(msg, "data"):
        battery_data["value"] = msg.data
        battery_data["power_supply_status"] = 3

    elif hasattr(msg, "rsoc"):
        battery_data["value"] = msg.rsoc
        battery_data["power_supply_status"] = 3

    return battery_data


def parse_goal(x, y, rotation_quaternion):
    # parse goal in ros format for sending navigation goal
    goal = PoseStamped()
    goal.header.stamp = rospy.Time.now()
    goal.header.frame_id = "map"
    goal.pose.position.x = float(x)
    goal.pose.position.y = float(y)
    goal.pose.position.z = 0.0
    goal.pose.orientation.x = float(rotation_quaternion["qx"])
    goal.pose.orientation.y = float(rotation_quaternion["qy"])
    goal.pose.orientation.w = float(rotation_quaternion["qw"])
    goal.pose.orientation.z = float(rotation_quaternion["qz"])
    return goal


def parse_cancel_goal(goal_id):
    goal = GoalID()
    goal.stamp = rospy.Time.now()
    goal.id = str(goal_id)
    return goal


def parse_waypoints(data):
    pose = []
    rotation_angle = data["rotation_angles"]
    waypoints = data["metric_waypoints"]

    for index, waypoint in enumerate(waypoints):
        x = waypoint[0]
        y = waypoint[1]
        rotation = rotation_angle[index]
        pose.append([x, y, rotation])
    pose.pop(0)

    return pose

def parse_vel (max_vel_x):

    # TO DO: get the current parameters and just change the linear.x
    vel = Twist()

    vel.linear.x = 0.0 #float(max_vel_x)
    vel.linear.y = 0.0
    vel.linear.z = 0.0

    vel.angular.x = 0.0
    vel.angular.y = 0.0
    vel.angular.z = 0.0

    return vel
