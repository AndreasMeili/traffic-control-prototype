import rospy
from nav_msgs.msg import OccupancyGrid

import os


class traffic_control:
    def __init__(self):
        rospy.init_node("traffic_control", anonymous=True)
        self.tasks = []
        self.map = None
        self.get_map()

    def task_handler(self, _, data: dict, vehicle: str):
        rospy.loginfo(
            f"TRAFFIC_CONTROLL task handler \n data:\n{data}\nvehicle: {vehicle}")
        self.tasks.append(
            (vehicle, (data["location"]["x"], data["location"["y"]])))
        rospy.loginfo(f"TRAFFIC_CONTROL tasks: \n {self.tasks}")

    def map_received(self, data):
        """callback function when a map is received"""
        rospy.loginfo(f"\n\nTRAFFIC_CONTROL got the map!\n")
        self.map = data

    def get_map(self):
        rospy.Subscriber("/map", OccupancyGrid, self.map_received)
        if self.map == None:
            rospy.spin()
        else:
            return
