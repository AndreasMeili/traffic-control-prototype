import logging
import os

import boto3
import rospy
from botocore.config import Config
from botocore.exceptions import ClientError

logger = logging.getLogger(__name__)
logging.getLogger("boto3").setLevel(logging.CRITICAL)
logging.getLogger("botocore").setLevel(logging.CRITICAL)


class ParameterStore:
    """
    AWS SSM Parameter store helper

    The class will help retrieving variables from AWS SSM, OS and ROSPY
    """

    def __init__(
        self,
        aws_access_key_id=None,
        aws_secret_access_key=None,
        aws_region="eu-north-1",
        env="dev-ros",
        use_os=True,
    ):
        self.env = env
        self.__aws_client = None
        self.__params = {}
        self.__use_os = use_os

        if aws_access_key_id is not None and aws_secret_access_key is not None:
            config = Config(region_name=aws_region)
            self.__aws_client = boto3.client(
                "ssm",
                config=config,
                aws_access_key_id=aws_access_key_id,
                aws_secret_access_key=aws_secret_access_key,
            )

    def get(self, key, default=None, check_remote=True, trim=False):
        if key in self.__params:
            return self.__params[key]

        value = self.__get_from_ssm(key)

        if value is None and self.__use_os:
            value = self.__get_from_os(key)

        if value is not None:
            self.__params[key] = value
            return value

        value = self.__get_from_ros(key)

        if value is not None:
            self.__params[key] = value
            return value

        return default

    def __get_from_os(self, key):
        return os.environ.get(key, None)

    def __get_from_ssm(self, key):
        key = self.__convert_key_to_path(key)
        try:
            return self.__aws_client.get_parameter(Name=key, WithDecryption=True)[
                "Parameter"
            ]["Value"]
        except ClientError as e:
            if e.response["Error"]["Code"] == "ParameterNotFound":
                return None
            raise e

    def __get_from_ros(self, key):
        return rospy.get_param(key)

    def __convert_key_to_path(self, key: str):
        return "/{}{}{}".format(
            self.env,
            "{}" if not key.startswith("/") else "",
            key.lower().replace("_", "/"),
        )
