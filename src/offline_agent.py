#!/usr/bin/env python3
import csv
import datetime
import os
import urllib
from os.path import expanduser, join

import numpy
import rospy
import yaml
from sentry_sdk import capture_exception, capture_message


def open_yaml(filename):
    try:
        with open(filename, "r", encoding="utf-8") as config:
            return yaml.safe_load(config)
    except OSError:
        capture_exception(OSError)
        rospy.loginfo("[OfflineAgent] %s does not exist", filename)
        raise


def config_time(task):
    time = [task["time"]]
    for i in range(len(time)):
        if len(time[i]) != 5:
            raise AssertionError("[OfflineAgent] Time data incorrect length")

    try:
        t = time[0].split(":")
        total_minutes = int(t[0]) * 60 + int(t[1]) * 1

    except BaseException as e:
        capture_exception(e)
        capture_message("[OfflineAgent] Time data incorrect")
        raise BaseException from e

    cron_split = []
    cron = 0

    if task["cron"] != "":
        cron_split.append(task["cron"].split())
        cron_raw = cron_split
        cron_time1 = cron_raw[0][0]
        if len(cron_time1) < 5:

            if len(cron_time1) == 1:
                cron = 0
            elif len(cron_time1) == 4:
                cron_str = cron_time1[2] + cron_time1[3]
                cron = int(cron_str)
            else:
                cron_str = cron_time1[2]
                cron = int(cron_str)

        elif len(cron_time1) > 5:
            if len(cron_time1) == 7:
                cron_str = cron_time1[5] + cron_time1[6]
                cron = int(cron_str)
            elif len(cron_time1) == 6:
                cron_str = cron_time1[5]
                cron = int(cron_str)
        else:
            cron=0

        new_time = total_minutes

        while cron != 0 and new_time < 1440:
            new_time = new_time + cron
            new_time = min(new_time, 1440)
            new_time_datetime = "{:02d}:{:02d}".format(*divmod(new_time, 60))
            time.append(new_time_datetime)
    else:
        cron_split.append("")

    return time


def get_vehicle_position(vehicle_list, vehicle_uuid):
    try:
        position = vehicle_list.index(vehicle_uuid)
    except ValueError:
        capture_exception(ValueError)
        rospy.logerr(
            f"[OfflineAgent] Vehicle {vehicle_uuid} is not in the list of vehicle task will be sent to the first on "
            f"the list",
            vehicle_uuid,
        )
        position = 0
    return position


def check_empty(variable, name):
    if not variable and variable != 0:
        rospy.logerr("[OfflineAgent] %s is empty" % (name,))
        capture_exception(AssertionError)
        raise AssertionError("[OfflineAgent] Empty", name)


def raise_type_error(message):
    rospy.logerr(message)
    capture_exception(TypeError)
    capture_message(message)

    raise TypeError(message)


def config_goal(task):
    goals = []
    for goal in task["task_preset"]["subtasks"]:
        point = goal[list(goal.keys())[1]]
        point_type = point["point_type"]
        if not isinstance(point_type, str):
            raise_type_error("[OfflineAgent] Point Type is not string")
        check_empty(point_type, "Offline Task, point_type")
        x = point["x"]
        y = point["y"]
        check_empty(x, "Offline Task, x")
        check_empty(y, "Offline Task, y")

        if not isinstance(x, float) or not isinstance(y, float):
            raise_type_error("[OfflineAgent] X or Y is not float")

        rotation = point["rotation"]
        check_empty(rotation, "Offline Task, rotation")
        if not isinstance(rotation, int) or (rotation > 361 or rotation < 0):
            raise_type_error("[OfflineAgent] Rotation is an invalid data")

        goals.append([x, y, rotation])

    return goals


def config_weekdays(task):
    week_days = numpy.zeros(7, dtype=int)
    weeks_days = []

    for day in task["week_days"]:
        if not isinstance(day, int):
            raise_type_error("[OfflineAgent] Day is not integer")

        if int(day) < 0 or int(day) > 7:
            raise_type_error("[OfflineAgent] Day must be between 0 to 6")

        week_days[int(day)] = 1
        weeks_days.append(int(day))

    # If no days are specified it is done for all the days
    if len(weeks_days) == 0:
        rospy.loginfo("[OfflineAgent] Offline task were no weekdays where specified")

        for day in range(7):
            week_days[int(day)] = 1
            weeks_days.append(int(day))

    return weeks_days


def config_loc_uuid(task):
    location_uuid = []

    for goal in task["task_preset"]["subtasks"]:

        point = goal[list(goal.keys())[1]]

        if not isinstance(point["uuid"], str):
            raise_type_error("[OfflineAgent] Uuid is not string")

        check_empty(point["uuid"], "Offline Task, point_uuid")

        location_uuid.append(point["uuid"])

    return location_uuid


class OfflineAgent:
    def __init__(
        self,
        agent_sdk_setup,
        number_of_vehicles,
        task_handler,
        waypoints_handler,
        vehicle_list,
        disc_buffer=20,
    ):
        self.last_executed_time = None
        self.client = None
        self.sdk_setup = agent_sdk_setup
        self.number_of_vehicles = number_of_vehicles
        self.task_handler = task_handler
        self.waypoints_handler = waypoints_handler
        self.vehicle_list = vehicle_list

        self.internet_down = 0
        self.offline = None
        self.ws_open = None

        self.disconnections = 0
        self.offline_tasks = None

        self.disc_buffer = disc_buffer

        self.current_task_uuid = None  # DOUBTS!

        rospy.loginfo("[OfflineAgent] Initialized offline agent")

    def check_internet_connection(self, offline, ws_open, host="https://google.com"):

        self.offline = offline
        self.ws_open = ws_open

        try:
            # CHECK FOR INTERNET CONNECTION

            urllib.request.urlopen(host, timeout=5) #nosec

            self.internet_down = 0
            if self.offline is not False:
                self.internet_connected()
            return self.offline, self.ws_open, self.disconnections

        except Exception as error:
            capture_message(f"[OfflineAgent] Internet Disconnected because of {error}")
        # The internet must be down disc_buffer (disc_buffer) times consecutive in a row
        # This avoids confusion with short internet disconnections
        if self.internet_down != self.disc_buffer:
            self.internet_down += 1
        if self.offline is not True:
            self.internet_not_connected()

        self.disconnections += 1

        return self.offline, self.ws_open, self.disconnections

    def internet_connected(self):
        self.offline = False

        rospy.loginfo("[OfflineAgent] Internet Connected")
        # START WS IF IT IS NOT ALREADY OPEN

        if not self.ws_open:
            try:
                self.client = self.sdk_setup()
                for index in self.vehicle_list:
                    self.client.add_vehicle(index)
                self.client.run_in_thread()
                self.ws_open = True

            except Exception as e:
                capture_exception(e)
                capture_message("[OfflineAgent] Error configuring ws")
                rospy.logerr("[OfflineAgent] Error configuring ws")

        # IF WS IS ALREADY OPEN SEND OFFLINE LOG
        elif self.ws_open:
            pass
            # To DO Send Offline Log in SDK
            # if self.ws.send_offlinelog():
            #    rospy.loginfo("[OfflineAgent] Offline Log Task file sent")

    def internet_not_connected(self):
        rospy.loginfo("[OfflineAgent] Internet Disconnected")
        rospy.loginfo("[OfflineAgent] Offline Agent Activated")
        self.offline = True
        # log_sent = False

    def config_offlinetasks(self):
        offline_tasks = []
        rospy.loginfo("[OfflineAgent] Configuring Offline Tasks")

        if self.offline:

            try:
                tasks_file_path = join(expanduser("~"), ".meili/tasks.yaml")
                data = open_yaml(tasks_file_path)
                check_empty(data, "Offline Tasks file")

                self.last_executed_time = [0 for _ in range(self.number_of_vehicles)]

                for task in data:
                    uuid = task["uuod"]
                    vehicle = task["vehicle"]["uuid"]

                    if not isinstance(uuid, str):
                        raise_type_error("[OfflineAgent] Offline Task file, task uuid")
                    if not isinstance(vehicle, str):
                        raise_type_error(
                            "[OfflineAgent] Offline Task file, vehicle uuid"
                        )

                    check_empty(uuid, "Offline Task file, uuid")
                    check_empty(vehicle, "Offline Task file, vehicle uuid")

                    goals = []
                    weeks_days = []
                    time = []
                    location_uuid = []
                    try:
                        weeks_days = config_weekdays(task)
                        goals = config_goal(task)
                        location_uuid = config_loc_uuid(task)
                        time = config_time(task)
                    except Exception as e:
                        rospy.logerr(
                            "[OfflineAgent] Configuration Error: %s %s"
                            % (type(e).__name__, e)
                        )

                    if len(vehicle) != 0 and len(goals) != 0:
                        offline_tasks.append(
                            [uuid, weeks_days, time, vehicle, goals, location_uuid]
                        )

            except Exception as error:
                capture_exception(AssertionError)
                capture_message("[OfflineAgent] Error Configuring Offline Tasks")
                rospy.logerr(f"[OfflineAgent] Error Configuring Offline Tasks: {error}")
                raise

        return offline_tasks

    def offline_task_handler(self, pose, location, vehicle_position):
        if len(pose) == 1:
            x = pose[0][0]
            y = pose[0][1]
            xm = x
            ym = y
            rotation = pose[0][2]
            vehicle = self.offline_tasks[0][3]

            task = {
                "location": location,
                "number": location,
                "uuid": location,
            }
            data = {
                "uuid": location,
                "number": location,
                "location": {
                    "uuid": location,
                    "x": x,
                    "y": y,
                    "metric": {"x": xm, "y": ym},
                    "rotation": rotation,
                },
                "metric_waypoints": None,
            }

            self.task_handler(
                task,
                data=data,
                vehicle=vehicle,
            )
        else:
            self.waypoints_handler(vehicle_position=vehicle_position, pose=pose, current_task_uuid=location)

    def offline_task_executer(self, offline_tasks, vehicle_list, task_offline_started):
        today = datetime.date.today().weekday()
        now = datetime.datetime.now()
        current_time = now.strftime("%H:%M")

        self.offline_tasks = offline_tasks

        for task in offline_tasks:

            task_execution_time = task[2]
            task_execution_day = task[1]

            pose = task[4]
            vehicle_uuid = task[3]
            location = task[5]
            position = get_vehicle_position(vehicle_list, vehicle_uuid)

            for day in task_execution_day:
                # task_offline_started = 1 # Refactoring
                if today == day:
                    for execution_time in task_execution_time:
                        self.current_task_uuid = task[0]  # master
                        if (
                            current_time == execution_time
                            and task_offline_started is False
                            and current_time != self.last_executed_time[position]
                        ):
                            task_offline_started = True
                            self.last_executed_time[position] = execution_time

                            self.offline_task_handler(
                                pose,
                                location,
                                position,
                            )

        return self.current_task_uuid, task_offline_started

    def logging_offlinetasks(
        self, offline_task, number_of_tasks, vehicle_list, start_time, success
    ):
        task_number = number_of_tasks
        uuid = offline_task[0]

        vehicle = offline_task[3]
        location_uuid = offline_task[5]

        time = self.last_executed_time[vehicle_list.index(vehicle)]
        now = datetime.datetime.now()
        end_time = now.strftime("%d/%m/%Y %H:%M:%S")

        tasks_log_file_path = join(expanduser("~"), ".meili/log_tasks.csv")
        file_exists = os.path.isfile(tasks_log_file_path)

        with open(tasks_log_file_path, "a+", newline="", encoding="utf-8") as file:
            fieldnames = [
                "no",
                "time",
                "uuid",
                "vehicle",
                "success",
                "location_uuid",
                "start_time",
                "end_time",
            ]
            writer = csv.DictWriter(file, fieldnames=fieldnames)
            if not file_exists:
                writer.writeheader()
            data = {
                "no": task_number,
                "time": time,
                "uuid": uuid,
                "vehicle": vehicle,
                "success": success,
                "location_uuid": location_uuid,
                "start_time": start_time,
                "end_time": end_time,
            }

            writer.writerow(data)
        rospy.loginfo(
            "[OfflineAgent] Task %s Logged in %s", task_number, tasks_log_file_path
        )
