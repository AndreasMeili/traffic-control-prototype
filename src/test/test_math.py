#!/usr/bin/env python3
# standart python3 libraries

import sys
from unittest import TestCase

import rostest

from maths import euler_to_quaternion, quaternion_to_euler


class MathTestCase(TestCase):
    def test_quaternion_to_euler(self):
        x = 1
        y = 2
        z = 3
        w = 90
        yaw = quaternion_to_euler(x, y, z, w)

        self.assertEqual(yaw, 92.6312268108878)

    def test_quaternion_to_euler_wrong_data(self):
        x = "a"
        y = 2
        z = 3
        w = 90
        with self.assertRaises(TypeError):
            yaw = quaternion_to_euler(x, y, z, w)

    def test_eueler_to_quatertion(self):
        roll = 0
        pitch = 90
        yaw = 0

        rotation_quaternion = euler_to_quaternion(roll, pitch, yaw)
        self.assertEqual(
            rotation_quaternion,
            {"qx": 0.0, "qy": 0.8509035245341184, "qz": 0.0, "qw": 0.5253219888177297},
        )

    def test_eueler_to_quatertion_wrong_data(self):

        roll = "12"
        pitch = 90
        yaw = 0

        with self.assertRaises(TypeError):
            rotation_quaternion = euler_to_quaternion(roll, pitch, yaw)


if __name__ == "__main__":
    rostest.rosrun("meili_agent", "test_math", MathTestCase, sys.argv)
