#!/usr/bin/env python3
import sys
import unittest
from os.path import abspath, dirname
from unittest import mock

import rostest

from config_agent import ConfigAgent


class ConfigAgentTestCase(unittest.TestCase):
    def setUp(self):
        self.c = ConfigAgent()
        self.c.filenametopics = (
            dirname(dirname(abspath(__file__))) + "/src/data/defaults.json"
        )
        self.c.filenamevehicles = (
            dirname(dirname(abspath(__file__))) + "/config/config.yaml"
        )

    def test_open_files_wrong_directory(self):

        with self.assertRaises(OSError) as cm:
            open_yaml("abc")
        the_exception = cm.exception
        self.assertEqual(the_exception.errno, 2)

        with self.assertRaises(OSError) as cm:
            open_json("abc")
        the_exception = cm.exception
        self.assertEqual(the_exception.errno, 2)

    @mock.patch("config_agent.ConfigAgent.open_yaml", return_value="")
    def test_open_files_empty_yaml_file(self, mock_open_yaml):
        with self.assertRaises(AssertionError):
            self.c.open_files()

    @mock.patch("config_agent.ConfigAgent.open_json", return_value="")
    def test_open_files_empty_json_file(self, mock_open_json):
        with self.assertRaises(AssertionError):
            self.c.open_files()

    def test_config_var_type(self):
        self.c.open_files()
        setup_token, server_instance, token, mqtt_id, fleet = self.c.config_var()

        self.assertIsInstance(setup_token, str)
        self.assertIsInstance(token, str)
        self.assertIsInstance(mqtt_id, str)
        self.assertIsInstance(fleet, bool)

    def test_config_var_length(self):
        self.c.open_files()
        setup_token, server_instance, token, mqtt_id, fleet = self.c.config_var()

        self.assertEqual(len(setup_token), 40)
        self.assertEqual(len(token), 40)
        self.assertEqual(len(mqtt_id), 48)

    def test_config_var_missing(self):

        self.c.cfg = {"uuid": "abc"}
        with self.assertRaises(KeyError):
            self.c.config_var()

        self.c.cfg = {"token": "bnm"}

        self.c.data = {"fleet": "123"}
        with self.assertRaises(KeyError):
            self.c.config_var()

        self.c.data = {"mqttid": "bnm"}
        with self.assertRaises(KeyError):
            self.c.config_var()

    def test_config_vehicles_missing_vehicles(self):
        with self.assertRaises(TypeError):
            self.c.set_vehicles_topics(None)

    def test_config_vehicles_length(self):
        self.c.open_files()
        vehicle_list, vehicles, vehicle, vehicle_tokens = self.c.config_vehicles()
        self.assertEqual(len(vehicle_list), len(vehicle_tokens))


if __name__ == "__main__":
    rostest.rosrun("meili_agent", "test_config_agent", ConfigAgentTestCase, sys.argv)
