#!/usr/bin/env python3

"""Principal Agent. Initialization of node"""
# standard python3 libraries
import logging
import signal
import sys
from importlib import import_module
from os import environ

import dynamic_reconfigure.client

# ROS1 dependencies
import rospy
from geometry_msgs.msg import Twist
from nav_msgs.msg import Path
from sentry_sdk import capture_exception

from agent import MeiliAgent, agent_sentry
from handlers import SDKHandlers
from offline_agent import OfflineAgent
from sentry import initialize_sentry
from utils.parameter_store import ParameterStore

# Meili agent dependencies


initialize_sentry()


class InitiationNode:
    def __init__(self):
        self.__aws_access_key_id = None
        self.__aws_secret_access_key = None
        self.path_planning = None
        self.offline_agent_flag = None
        self.lipo_battery = None
        self.traffic_control = None
        self.battery_present = False
        self.topic_streaming_mode = None
        self.publish_frequency = None

        rospy.init_node("agent")
        self.init_parameters_setup()
        self.parameter_store = ParameterStore(
            self.__aws_access_key_id, self.__aws_secret_access_key
        )

    def init_parameters_setup(self):
        # get initial ros parameters
        try:
            if (
                    environ.get("AWS_ACCESS_KEY_ID") is not None
                    and environ.get("AWS_SECRET_ACCESS_KEY") is not None
            ):
                self.__aws_access_key_id = environ.get("AWS_ACCESS_KEY_ID")
                self.__aws_secret_access_key = environ.get("AWS_SECRET_ACCESS_KEY")
            else:
                self.__aws_access_key_id = None
                self.__aws_secret_access_key = None

            ros_distro = rospy.get_param("/rosdistro")  # get ROS distro
            ros_version = rospy.get_param("/rosversion")  # get ros version

            # ROS1
            system_id = rospy.get_param("/run_id")  # get system id
            #

            rospy.loginfo("[ROSAgent] Reading initialization parameters")
            rospy.loginfo("[ROSAgent] Ros distro is : %s", ros_distro)
            rospy.loginfo("[ROSAgent] Ros version is : %s", ros_version)

            # ROS1
            rospy.loginfo("[ROSAgent] System ID is : %s", system_id)
            #

            self.get_parameters()

        except KeyError as error:
            rospy.logerr("[ROSAgent] Parameter %s does not exist. ", error)
            agent_sentry(error)
            rospy.logerr("[ROSAgent] Roscore is not running, check your ros config")
            raise

        rospy.loginfo("[ROSAgent] COMPLETE Initial ROS Parameter configuration")

    def get_parameters(self):
        try:
            self.publish_frequency = rospy.get_param("/publish_frequency")
            self.topic_streaming_mode = rospy.get_param("/topic_streaming_mode")
            battery = rospy.get_param("battery_present")
            self.traffic_control = rospy.get_param("traffic_control")
            self.offline_agent_flag = rospy.get_param("offline_agent")
            self.path_planning = rospy.get_param("path_planning")

            if battery == "lipo":
                self.battery_present = True
                self.lipo_battery = True
            elif battery == "yes":
                self.battery_present = True

        except KeyError as error:
            rospy.logerr("[ROSAgent] Parameter %s does not exist. ", error)
            capture_exception(error)
            raise


def get_message_type(message_type):
    try:
        assert sys.version_info >= (2, 7)  # import_module's syntax needs 2.7
        connection_header = message_type.split("/")
        ros_pkg = connection_header[0] + ".msg"
        msg_type = connection_header[1]
        msg_class = getattr(import_module(ros_pkg), msg_type)
        return msg_class
    except Exception as error:
        rospy.logerr(f"[ROSAgent] Error when getting message type: {error}")
        raise


def main():
    # mqtt logging active
    logging.basicConfig(level=logging.DEBUG)

    # SETUP

    node = InitiationNode()
    agent = MeiliAgent(node)
    # init ros node
    handlers = SDKHandlers(agent)
    rate = rospy.Rate(5.0)

    agent.offline_agent = OfflineAgent(
        handlers.sdk_setup,
        agent.number_of_vehicles,
        handlers.task_handler,
        handlers.waypoints_handler,
        agent.vehicle_list,
    )
    ################################################

    rate.sleep()
    rate.sleep()

    gps_msg = get_message_type(agent.vehicles[0]["topics"]["gps"]["messageType"])
    battery_msg = get_message_type(
        agent.vehicles[0]["topics"]["battery"]["messageType"]
    )
    pose_msg = get_message_type(agent.vehicles[0]["topics"]["pose"]["messageType"])
    goalArray_msg = get_message_type(
        agent.vehicles[0]["topics"]["goalArray"]["messageType"]
    )
    goal_msg = get_message_type(agent.vehicles[0]["topics"]["goal"]["messageType"])
    goalCancel_msg = get_message_type(
        agent.vehicles[0]["topics"]["goal_cancel"]["messageType"]
    )

    # SUBSCRIBERS AND PUBLISHERS
    # ROS1
    # config topics, publishers and subscriptions
    for index, vehicle in enumerate(agent.vehicles):
        try:
            rospy.Subscriber(
                vehicle["topics"]["battery"]["topic"],
                battery_msg,
                agent.callback_battery,
                vehicle["uuid"],
                tcp_nodelay=True,
            )

            rospy.Subscriber(
                vehicle["topics"]["pose"]["topic"],
                pose_msg,
                agent.callback_pose,
                vehicle["uuid"],
                tcp_nodelay=True,
            )

            rospy.Subscriber(
                vehicle["topics"]["goalArray"]["topic"],
                goalArray_msg,
                agent.callback_status,
                vehicle["uuid"],
                tcp_nodelay=True,
            )

            agent.goal_publisher.append(
                rospy.Publisher(
                    vehicle["topics"]["goal"]["topic"], goal_msg, queue_size=5
                )
            )
            agent.goal_canceller.append(
                rospy.Publisher(
                    vehicle["topics"]["goal_cancel"]["topic"],
                    goalCancel_msg,
                    queue_size=5,
                )
            )

            if agent.outdoor:
                rospy.Subscriber(
                    vehicle["topics"]["gps"]["topic"],
                    gps_msg,
                    agent.callback_gps,
                    vehicle["uuid"],
                    tcp_nodelay=True,
                )

            if node.traffic_control:
                rospy.loginfo("[ROSAgentNode] Traffic Control Activated")
                vehicle_prefix = str(vehicle["prefix"])
                if vehicle_prefix == "None":
                    vehicle_prefix = ""
                    prefix_mod2 = ""
                else:
                    prefix_mod2 = vehicle_prefix[1: len(vehicle_prefix)]
                    if agent.number_of_vehicles >= 2:
                        prefix_mod2 = "_" + prefix_mod2

                agent.dynamic_reconfigure_client.append(
                    dynamic_reconfigure.client.Client(
                        vehicle_prefix + "/move_base" + prefix_mod2 + "/DWAPlannerROS"
                    )
                )
                agent.max_vel_x[index] = rospy.get_param(
                    vehicle_prefix
                    + "/move_base"
                    + prefix_mod2
                    + "/DWAPlannerROS"
                    + "/max_vel_x"
                )
                agent.max_vel_theta[index] = rospy.get_param(
                    vehicle_prefix
                    + "/move_base"
                    + prefix_mod2
                    + "/DWAPlannerROS"
                    + "/min_vel_theta"
                )

                agent.cmd_vel_publisher.append(rospy.Publisher(vehicle_prefix + "/cmd_vel", Twist, queue_size=5))

                rospy.Subscriber(
                    vehicle_prefix + "/move_base" + prefix_mod2 + "/NavfnROS/plan",
                    Path,
                    agent.callback_plan,
                    vehicle["uuid"],
                    tcp_nodelay=True,
                )


        except Exception as error:
            capture_exception(error)
            rospy.loginfo(
                "[ROSAgentNode] Error when creating the subscribers and publishers"
            )
            rospy.loginfo("[ROSAgentNode] %s", error)
            raise

    rate.sleep()
    rospy.loginfo("[ROSAgentNode] Topics are subscribed")
    rospy.loginfo("[ROSAgentNode] Meili Agent is ready for receiving tasks")

    agent.run()
    rospy.spin()


def exit_gracefully(_, __):
    """Exiting gracefully the program"""
    sys.exit(0)


if __name__ == "__main__":
    try:
        signal.signal(signal.SIGINT, exit_gracefully)
        main()
    except Exception as e:
        capture_exception(e)
