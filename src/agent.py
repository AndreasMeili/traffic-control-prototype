#!/usr/bin/env python3
# standard python3 libraries
""" Agent Management"""
import datetime
import sys

# ROS1 dependencies
import rospy
from geometry_msgs.msg import PoseWithCovarianceStamped, Pose
from meili_agent.msg import Topicinit
from meili_sdk.websockets import constants
from meili_sdk.websockets.models.message import Message
from sensor_msgs.msg import NavSatFix

# Meili agent dependencies
from sentry_sdk import capture_exception, capture_message

from config_agent import ConfigAgent
from maths import quaternion_to_euler
from parse_data import parse_battery_data
from sentry import initialize_sentry

initialize_sentry()


def update_capacity_lipo(voltage):
    """Calculate current capacity of lipo batteries taking into account ...
    voltage/capacity estimation using regression capacity = 47.6 *voltage - 500"""
    capacity = 47.6 * voltage - 500
    return capacity


def agent_sentry(exception=None, message=None):
    if message is not None:
        capture_message(message)
    if exception is not None:
        capture_exception(exception)


def topic_streaming_talker(msg):
    """Publish Ros Message to topic streaming node if there is topics to stream"""
    pub = rospy.Publisher("/topic_init", Topicinit, queue_size=10)
    rate = rospy.Rate(1)  # 10hz
    # wait for having the topic streaming node subscribe
    while pub.get_num_connections() < 1:
        pass
    pub.publish(msg)
    rate.sleep()


class MeiliAgent:
    """
    Class for interact with ROS and Meili Cloud.
    """

    def __init__(self, node):

        # initial instances
        self.node = node
        self.token = None
        self.fleet = None
        self.mqtt_id = None

        self.vehicle_list = []
        self.vehicle_tokens = []
        self.vehicles = {}
        self.number_of_vehicles = None

        # other classes
        self.offline_agent = None
        self.client = None

        # messages
        self.msg_pose = []
        self.msg_battery = []
        self.msg_gps = []

        self.msg_plan = []
        self.arr_plan = []
        self.buffer = []

        # publishers and subscribers
        self.goal_publisher = []
        self.goal_canceller = []

        # internet variables
        self.ws_open = False
        self.offline = None
        self.current_task_status_id = 0  # This variable is used in refactoring
        self.disconnections = None

        # task variables
        self.task_offline_started = False
        self.task_started = []
        self.offline_tasks = []
        self.current_task_uuid = None
        self.number_of_tasks = 0
        self.start_time = None

        # waypoints variables
        self.waypoints = []
        self.waypoints_goal_id = []

        # traffic control
        self.dynamic_reconfigure_client = []
        self.goal_id = None
        self.goal_id_speed_mod = []
        self.max_vel_x = []
        self.max_vel_theta = []
        self.speed_changed = []
        self.vehicle_count_speed_mod = []
        self.cmd_vel_publisher = []
        self.path_length = 200

        # miscellaneous variables
        self.total_topics = 0
        self.outdoor = False

        # Variables not in refactoring
        # self.number_of_vehicles = None
        self.disconnections = None

        self.vehicles = self.__agent_setup()

    # SETTING UP FUNCTIONS

    def __agent_setup(self):

        config = ConfigAgent()
        rospy.loginfo("[ROSAgent] The configuration is started...")

        try:
            config.open_files()

            (
                setup_token,
                self.server_instance,
                self.token,
                self.mqtt_id,
                self.fleet,
            ) = config.config_var()

            rospy.loginfo(f"[ROSAgent] Server instance:{self.server_instance}")
            (
                self.vehicle_list,
                self.vehicles,
                self.vehicle,
                self.vehicle_tokens,
            ) = config.config_vehicles()

            self.number_of_vehicles = len(self.vehicle_list)

            for i in range(0, self.number_of_vehicles):
                self.msg_pose.append(i)
                self.msg_battery.append(i)
                self.task_started.append(0)
                self.waypoints.append(False)
                self.waypoints_goal_id.append(None)

                self.max_vel_x.append(0)
                self.max_vel_theta.append(0)
                self.vehicle_count_speed_mod.append(0)
                self.speed_changed.append(0)
                self.goal_id_speed_mod.append(0)

                self.msg_plan.append(i)
                self.arr_plan.append([])
                self.buffer.append(i)

                # ROS1
                self.msg_gps.append(i)

        except Exception as error:
            rospy.logerr(
                "[ROSAgent] Agent Setup Error: %s %s" % (type(error).__name__, error)
            )
            agent_sentry(error, "[ROSAgent] Agent Setup Error")
            sys.exit()

        for vehicle in self.vehicles:
            rospy.loginfo("[ROSAgent] Vehicle %s is on the fleet", vehicle["uuid"])
        rospy.loginfo("[ROSAgent] Agent has been configured")

        return self.vehicles

    # PING FUNCTIONS

    def ping_location(self):
        """Ping Location"""
        try:
            count = 0
            for msg in self.msg_pose:
                if isinstance(msg, PoseWithCovarianceStamped):
                    x = round(msg.pose.pose.position.x, 3)
                    y = round(msg.pose.pose.position.y, 3)
                    orientation = msg.pose.pose.orientation
                    yaw = round(
                        quaternion_to_euler(
                            orientation.x, orientation.y, orientation.z, orientation.w
                        ),
                        3,
                    )
                    message = Message(
                        event=constants.EVENT_LOCATION,
                        value={"xm": x, "ym": y, "rotation": yaw},
                        vehicle=self.vehicle_list[count],
                    )
                    message.validate()
                    self.client.send(message)
                elif isinstance(msg, Pose):
                    x = round(msg.position.x, 3)
                    y = round(msg.position.y, 3)
                    orientation = msg.orientation
                    yaw = round(
                        quaternion_to_euler(
                            orientation.x, orientation.y, orientation.z, orientation.w
                        ),
                        3,
                    )
                    message = Message(
                        event=constants.EVENT_LOCATION,
                        value={"xm": x, "ym": y, "rotation": yaw},
                        vehicle=self.vehicle_list[count],
                    )
                    message.validate()
                    self.client.send(message)

                else:
                    rospy.loginfo("[ROSAgent] Robot pose is not available")
                    capture_message("[ROSAgent] Robot pose is not available")
                count = count + 1
        except Exception as e:
            rospy.loginfo("[ROSAgent] Ping Location Error: %s ", e)
            capture_exception(e)

    def ping_battery(self):
        """Ping Battery"""
        try:
            for i, msg in enumerate(self.msg_battery):
                if self.node.lipo_battery:
                    msg.capacity = update_capacity_lipo(msg.voltage)

                battery_data = parse_battery_data(msg)
                message = Message(
                    event=constants.EVENT_BATTERY,
                    value={**battery_data},
                    vehicle=self.vehicle_list[i],
                )
                message.validate()
                self.client.send(message)

        except Exception as e:
            rospy.loginfo("[ROSAgent] Ping Battery Error: %s ", e)
            capture_exception(e)

    def ping_trajectory(self):
        # send the trajectory as an array of poses
        if self.number_of_tasks > 0:
            try:
                count = 0
                for index in self.arr_plan:

                    if index not in (0, [],self.buffer[count]):
                        self.buffer[count] = index
                        path = index
                        message = Message(
                            event=constants.EVENT_PATH_DATA,
                            value={"path": path},
                            vehicle=self.vehicle_list[count],
                        )
                        message.validate()
                        self.client.send(message)
                    count += 1
            except Exception as e:
                rospy.loginfo("[ROSAgent] Ping Trajectory Error: %s ", e)
                capture_exception(e)

    def ping_gps(self):
        try:
            if self.outdoor:
                for i, index in enumerate(self.msg_gps):
                    if type(index) is NavSatFix:
                        lat = index.latitude
                        lon = index.longitude
                        rotation = 0
                        message = Message(
                            event=constants.EVENT_LOCATION,
                            value={"lat": lat, "lon": lon, "rotation": rotation},
                            vehicle=self.vehicle_list[i],
                        )
                        message.validate()
                        self.client.send(message)

        except Exception as e:
            rospy.loginfo("[ROSAgent] Ping GPS Error: %s ", e)
            capture_exception(e)

    ################################################

    def return_vehicle_position(self, vehicle_token):
        count = 0
        vehicle_position = None
        for vehicle in self.vehicle_list:
            if vehicle == vehicle_token:
                vehicle_position = count
            count = count + 1

        return vehicle_position

    # CALLBACKS

    def callback_pose(self, msg, vehicle_uuid):
        # Parse current robot pose and sent to FMS task_msg_server

        vehicle_position = self.return_vehicle_position(vehicle_uuid)
        self.msg_pose[vehicle_position] = msg

        if self.node.topic_streaming_mode == "REALTIME" and self.ws_open:
            self.ping_location()

    def callback_battery(self, msg, vehicle_uuid):
        # When battery topic is available, is parsed and sent to FMS task_msg_server

        if self.node.battery_present:
            vehicle_position = self.return_vehicle_position(vehicle_uuid)
            self.msg_battery[vehicle_position] = msg

            if self.node.topic_streaming_mode == "REALTIME" and self.ws_open:
                self.ping_battery()

    def task_in_progress(self, goal_id, status_id, _, vehicle_uuid):
        # TASK ONLINE
        if self.ws_open:
            message = Message(
                event=constants.EVENT_GOAL_STATUS,
                value={"goal_id": goal_id, "status_id": status_id},
                vehicle=vehicle_uuid,
            )

            message.validate()
            self.client.send(message)

        # TASK OFFLINE
        else:
            now = datetime.datetime.now()
            dt_string = now.strftime("%d/%m/%Y %H:%M:%S")
            self.start_time = dt_string

        return 1

    def task_finished(self, goal_id, status_id, _, vehicle_uuid):
        vehicle_position = self.return_vehicle_position(vehicle_uuid)
        # TASK ONLINE
        if self.ws_open:
            # SUCCESSFUL TASK ONLINE with change of speed

            if status_id == 3 and self.goal_id_speed_mod[vehicle_position] == goal_id:
                # REVERTING SPEED TO ORIGINAL
                self.revert_slow_down_handler(
                    count=self.vehicle_count_speed_mod[vehicle_position]
                )

            if not self.waypoints[vehicle_position]:
                if self.waypoints_goal_id[vehicle_position] is not None:
                    goal_id = self.waypoints_goal_id[vehicle_position]
                message = Message(
                    event=constants.EVENT_GOAL_STATUS,
                    value={"goal_id": goal_id, "status_id": status_id},
                    vehicle=self.vehicle_list[vehicle_position],
                )
                message.validate()

                self.client.send(message)
                self.waypoints_goal_id[vehicle_position] = None
                self.task_started[vehicle_position] = 0

            # rospy.loginfo("[ROSAgent] Task online terminated")

        # TASK OFFLINE
        elif self.offline and self.current_task_uuid is not None:
            # rospy.loginfo("[ROSAgent] Task offline terminated")
            success = True

            for task in self.offline_tasks:
                if self.current_task_uuid == task[0]:
                    # and self.current_task_uuid is not None:
                    self.task_started[vehicle_position] = 0

                    if not self.waypoints[vehicle_position]:
                        self.offline_agent.logging_offlinetasks(
                            task,
                            self.number_of_tasks,
                            self.vehicle_list,
                            self.start_time,
                            success=success,
                        )
                        self.start_time = None
                        self.task_offline_started = False
                        # self.current_task_uuid = None Refactoring
        return 0

    def callback_status(self, msg, vehicle_uuid):
        # Status of navigation task is parsed and send to FMS task_msg_server
        if msg.status_list and len(msg.status_list) != 0:

            vehicle_position = self.return_vehicle_position(vehicle_uuid)

            last_status = msg.status_list[len(msg.status_list) - 1]
            goal_id = last_status.goal_id.id
            status_id = last_status.status
            status_text = last_status.text

            if (
                self.waypoints_goal_id[vehicle_position] is None
                and self.waypoints[vehicle_position]
            ):
                self.waypoints_goal_id[vehicle_position] = last_status.goal_id.id

            self.current_task_status_id = status_id
            if self.speed_changed[vehicle_position]:
                self.goal_id_speed_mod[vehicle_position] = goal_id

            # IN PROGRESS
            if status_id == 1 and self.task_started[vehicle_position] == 0:
                try:
                    self.task_started[vehicle_position] = self.task_in_progress(
                        goal_id, status_id, status_text, vehicle_uuid
                    )
                except Exception as e:
                    rospy.logerr(
                        "[ROSAgent] Task in progress Callback Status error : %s", e
                    )
                    agent_sentry(e, "[ROSAgent] Task in progress Callback Status error")

            # IF EXECUTING; SUCCEEDED; ABORTED or CANCELED
            if (status_id in (2, 3, 4, 5)) and self.task_started[vehicle_position] == 1:

                try:
                    self.task_started[vehicle_position] = self.task_finished(
                        goal_id, status_id, status_text, vehicle_uuid
                    )

                except Exception as e:
                    rospy.logerr(
                        "[ROSAgent] Task finished Callback Status error: %s", e
                    )
                    agent_sentry(e, "[ROSAgent] Task finished Callback Status error")

    def callback_plan(self, msg, vehicle_token):
        """Callback Plan"""
        # Parse trajectory and send to the fms

        arr = [[pose.pose.position.x, pose.pose.position.y] for pose in msg.poses]
        vehicle_position = self.return_vehicle_position(vehicle_token)
        if len(arr)>self.path_length:
            self.arr_plan[vehicle_position] = arr[0:self.path_length]
        else:
            self.arr_plan[vehicle_position]=arr

        if (
            self.node.topic_streaming_mode == "REALTIME"
            and self.ws_open
            and self.node.traffic_control
        ):
            self.ping_trajectory()

    def callback_gps(self, msg, vehicle_token):
        """Callback GPS"""
        vehicle_position = self.return_vehicle_position(vehicle_token)
        self.msg_gps[vehicle_position] = msg
        # Parse current robot pose and sent to FMS task_msg_server
        if self.node.topic_streaming_mode == "REALTIME" and self.ws_open:
            self.ping_location()

    ################################################
    def revert_slow_down_handler(self, count):
        """Revert Slow down"""
        params = {"max_vel_x": self.max_vel_x[count]}
        self.dynamic_reconfigure_client[count].update_configuration(params)
        self.speed_changed[count] = False

    def check_topic_streaming_mode(self):
        """Check Topic Streaming"""
        if self.node.topic_streaming_mode == "FILTERING":
            rospy.loginfo("[ROSAgent] Mode for topic streaming is FILTERING")
        else:
            rospy.loginfo("[ROSAgent] Mode for topic streaming is REALTIME")

    def check_internet(self):
        """Check if there is internet if not connect the offline agent"""
        (
            self.offline,
            self.ws_open,
            self.disconnections,
        ) = self.offline_agent.check_internet_connection(self.offline, self.ws_open)

        if self.disconnections == 3:
            self.offline_tasks = self.offline_agent.config_offlinetasks()

    def ping(self):
        """Ping location and battery every second"""
        pub_rate = rospy.Rate(self.node.publish_frequency)
        if self.node.topic_streaming_mode == "FILTERING" and self.ws_open:
            pub_rate.sleep()
            if self.outdoor:
                self.ping_gps()
            else:
                self.ping_location()

            if self.node.battery_present:
                self.ping_battery()

            if self.node.traffic_control:
                self.ping_trajectory()

    def run(self):
        """Running the agent"""
        # When Mode is filtering, all data is sent to the FMS task_msg_server at specific frequency
        rate = rospy.Rate(1)
        self.check_topic_streaming_mode()
        while True:
            try:
                # Check internet connection
                if not self.task_offline_started and self.node.offline_agent_flag:
                    self.check_internet()
                    rate.sleep()
                elif not self.ws_open:
                    self.offline_agent.internet_connected()
                # Activate offline agent if no internet connection
                if self.offline:
                    try:
                        (
                            self.current_task_uuid,
                            self.task_offline_started,
                        ) = self.offline_agent.offline_task_executer(
                            self.offline_tasks,
                            self.vehicle_list,
                            self.task_offline_started,
                        )
                        rate.sleep()
                    except Exception as error:
                        rospy.loginfo(
                            "[ROSAgent] Warning in the offline task executer %s ", error
                        )

                self.ping()

            except Exception as error:
                capture_exception(error)
                rospy.logerr(f"[ROSAgent] Run Error: {type(error).__name__} {error}")
