import json
import logging
import threading

import paho.mqtt.client as mqtt
from sentry_sdk import capture_exception

TOPIC_STREAMING_TOPIC = "meili/topics/ros/{topic}/{vehicle}"
LOCATION_STREAMING_TOPIC = "meili/location/ros/{vehicle}"
BATTERY_STREAMING_TOPIC = "meili/battery/ros/{vehicle}"


class MQTTClient:
    """
    Meili MQTT client
    """

    open_handler = None
    close_handler = None

    def __init__(self, client_id=""):
        # Setup MQTT client
        self.__client = mqtt.Client(client_id=client_id)
        self.__client.on_connect = self.__on_connect
        # self.__client.on_message = self.__on_message
        self.__client.on_disconnect = self.__on_disconnect

        self.__thread: threading.Thread = None

    def __on_connect(self, client, userdata, flags, rc):
        logging.info("MQTT connection open")
        if self.open_handler is not None:
            self.open_handler(client, userdata, flags, rc)

    def __on_message(self, client, userdata, msg):
        # TODO implement this
        topic = msg.topic
        payload = msg.payload
        try:
            data = json.loads(payload)
        except Exception as e:
            capture_exception(e)
            print(e)

    def __on_disconnect(self, client, userdata, rc):

        if self.close_handler is not None:
            self.close_handler(client, userdata, rc)

    def send_topic_data(
        self,
        topic_uuid: str,
        data: dict,
        vehicle_token: str,
        message_type: str,
        topic_name: str,
        block=False,
    ):
        """
        Send topic data to the server
        """
        mqtt_topic = TOPIC_STREAMING_TOPIC.format(
            topic=topic_uuid, vehicle=vehicle_token
        )
        data = {
            "topic": topic_name,
            "message_type": message_type,
            "vehicle_token": vehicle_token,
            "data": {**data},
        }

        return self.__send_data(json.dumps(data), mqtt_topic, block=block)

    def send_location_data(self, vehicle_token: str, x, y, block=False):
        """
        Send metric location data to the server
        """
        mqtt_topic = LOCATION_STREAMING_TOPIC.format(vehicle=vehicle_token)

        data = {"vehicle": vehicle_token, "value": {"xm": x, "ym": y}}
        # print(data)

        return self.__send_data(json.dumps(data), mqtt_topic, block=block)

    def send_battery_data(
        self,
        vehicle_token,
        value=None,
        voltage=None,
        charge=None,
        capacity=None,
        power_supply_status=None,
        power_supply_health=None,
        present=None,
        current=None,
        percentage=None,
        cell_voltage=None,
        block=False,
    ):
        """
        Send battery data to the server
        """
        mqtt_topic = BATTERY_STREAMING_TOPIC.format(vehicle=vehicle_token)

        data = {
            "value": value,
            "voltage": voltage,
            "charge": charge,
            "capacity": capacity,
            "power_supply_status": power_supply_status,
            "power_supply_health": power_supply_health,
            "present": present,
            "current": current,
            "percentage": percentage,
            "cell_voltage": cell_voltage,
        }

        return self.__send_data(json.dumps(data), mqtt_topic, block=block)

    def __send_data(self, data: str, topic: str, qos: int = 0, block=False):
        """
        Publish message to MQTT server. Set block=True to block the flow
        """
        if not self.__client.is_connected():
            raise Exception("MQTT not connected")

        if block:
            return self.__publish_message(data, topic, qos)

        thread = threading.Thread(
            target=self.__publish_message, args=[data, topic, qos]
        )
        thread.start()
        return None

    def __publish_message(self, data: str, topic: str, qos: int = 0):
        request = self.__client.publish(topic, data, qos=qos)
        request.wait_for_publish()

        logging.info("Message publish status: {}".format(request.is_published()))

        return request.is_published()

    def start(self, block=False, host="13.51.64.167"):
        """
        Start MQTT client

        Pass block boolean to block thread.
        """
        if self.__client.is_connected():
            return

        self.__client.connect(host, keepalive=10)

        if block:
            self.__client.loop_forever()
        else:
            self.__thread = threading.Thread(target=self.__client.loop_forever, args=[])
            self.__thread.start()
