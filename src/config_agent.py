#!/usr/bin/env python3


import copy
import json
import logging
import os
import sys
from os.path import abspath, dirname, join

import rospy
import yaml
from sentry_sdk import capture_exception


def check_empty(variable, name):
    if not variable:
        rospy.logerr(f"[Config] {name} is empty")
        capture_exception(AssertionError)
        raise AssertionError("Empty", name)


def open_yaml(filename):
    try:
        with open(filename, "r", encoding="utf-8") as config:
            return yaml.safe_load(config)
    except OSError:
        rospy.logerr(f"[Config] {filename} does not exist")
        capture_exception(OSError)
        logging.exception(OSError)
        raise


def open_json(filename):
    try:
        with open(filename, "r", encoding="utf-8") as t:
            return json.load(t)
    except OSError:
        rospy.logerr(f"[Config] {filename} does not exist")
        capture_exception(OSError)
        logging.exception(OSError)
        raise


class ConfigAgent:
    """
    Class to configure the initial values of the meili_agent
    """

    def __init__(self):

        # config agent by reading and storing config files
        # about number of vehicles, prefixes and topics
        self.cfg_file_path = os.path.join(os.path.expanduser("~"), ".meili", "cfg.yaml")

        if len(sys.argv) < 1:
            rospy.loginfo("[Config]usage: agent.py pathToConfigTopics")
            sys.exit()
        else:
            config_topics_path = sys.argv[1]
            config_vehicles_path = sys.argv[2]

        self.dirname = dirname(__file__)
        dir_topics = self.dirname
        dir_config = dirname(dirname(abspath(__file__)))
        self.file_name_topics = abspath(join(dir_topics, config_topics_path))
        self.file_name_vehicles = abspath(join(dir_config, config_vehicles_path))

        self.data = None
        self.topics = None
        self.cfg = None

    def open_files(self):
        # Parse Mode, number of vehicles and tokens from config files passed as arguments
        self.topics = open_json(self.file_name_topics)
        self.data = open_yaml(self.file_name_vehicles)
        self.cfg = open_yaml(self.cfg_file_path)

        check_empty(self.topics, "Topics file")
        check_empty(self.data, "Data file")
        check_empty(self.cfg, "Config file")

    def config_var(self):
        try:
            setup_token = self.cfg["token"]
            server_instance = self.cfg["site"]
            token = self.data["fleet"]

            mqtt_id = self.data["mqtt_id"]
            rospy.set_param("/mqtt_id", mqtt_id)

            fleet = True
        except KeyError as error:
            rospy.logerr(f"[Config] Configuration of variable {error} error")
            capture_exception(error)
            raise

        check_empty(setup_token, "Config file, token")
        check_empty(server_instance, "Config file, server_instance")
        check_empty(token, "Data file, fleet")
        check_empty(mqtt_id, "Data file, mqtt_id")

        mode = "Fleet"
        rospy.loginfo("[ROSAgent] Fleet mode is : %s", mode)

        return setup_token, server_instance, token, mqtt_id, fleet

    def set_vehicles_topics(self, vehicles):
        try:
            for vehicle in vehicles:
                t = copy.deepcopy(self.topics.get("topics", {}))
                t.update(vehicle.get("topics", {}))
                for topic in t:
                    if (vehicle["prefix"]) is not None:
                        t[topic]["topic"] = "{}{}".format(
                            vehicle["prefix"], t[topic]["topic"]
                        )
                    else:
                        t[topic]["topic"] = "{}".format(t[topic]["topic"])

                vehicle["topics"] = t
            return vehicles

        except TypeError:
            rospy.logerr("[Config] Error setting the vehicles topics")
            capture_exception(TypeError)
            raise

    def config_vehicles(self):

        check_empty(self.data["vehicles"], "Data file, vehicles")
        vehicles = self.set_vehicles_topics(self.data["vehicles"])

        vehicle_list = []
        vehicle_tokens = []
        vehicle = None
        try:
            # creating sublist of vehicles and tokens
            for vehicle in vehicles:
                vehicle_list.append(vehicle["uuid"])
                vehicle_tokens.append(vehicle["token"])
        except KeyError as error:
            rospy.logerr(f"[Config] Configuration of variable {error} error")
            capture_exception(error)
            raise

        # checking out number of vehicles for choosing btw individual or fleet
        if len(vehicle_list) == 1:
            vehicle = self.data["vehicles"][0]["uuid"]

        return vehicle_list, vehicles, vehicle, vehicle_tokens
